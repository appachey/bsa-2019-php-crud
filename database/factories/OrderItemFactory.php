<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product' => $faker->numberBetween(1, 20),
        'quantity' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(100, 10000),
        'discount' => $faker->randomFloat(1, 0, 1)
    ];
});
