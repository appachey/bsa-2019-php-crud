<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['product','quantity','price', 'discount', 'order'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
